import json
import requests
import yaml
import base64
import imaplib
import email
import time
from email.header import decode_header


def send_message(text_to_send, send_to):
    #opening file to get Secrets
    with open ("SECRETS.yaml") as creds:
        secrets = yaml.safe_load(creds)

    url = secrets['endpoint']
    naming = secrets['naming']

    #Gettin credentials as string and encodeing to bytes
    credentials = f"{secrets['login']}:{secrets['password']}".encode('ascii')

    #converting Encoding bytes by base64
    base64Creds = base64.b64encode(credentials)

    #decoding bytes back to string
    creds = base64Creds.decode('ascii')


    with open ('payload.json') as payload:
        dataStr = json.loads(payload.read())
    json_message = dataStr['messages'][0]
    text_to_send = json_message['content']['short_text']
    namiong = json_message['from']['sms_address']
    send_to = json_message['to'][0]['msisdn']

    #converting back to JSON
    data = json.dumps(dataStr)

    header = {
        'Content-type': 'application/json',
        'authorization': f'Basic {creds}'
    }

    req = requests.post(url, data=data, headers=header)
    return req


def check_mail():
    payload = None
    with open ('SECRETS.yaml') as credentials:
        creds = yaml.safe_load(credentials)
    login = creds['mailLogin']
    password = creds['mailPW']
    #logging in to mail
    imap = imaplib.IMAP4_SSL(creds['imap_server'])
    imap.login(login, str(password))
    imap.select('INBOX')
    #Getting all UNSEEN msgs
    typel, data = imap.search(None, "UNSEEN", 'FROM SENDER@gmail.com')  # Add UNSEEN to get only unseen msgs

    #iterating through all the msgs
    payload = [] #Creating empty list to add all the msgs if we have some simultaniously
    for num in data[0].split():
        #Getting one of the latest msgs
        typ, mail = imap.fetch(num, ('RFC822'))
        #convertin to text from bytes
        mail = email.message_from_bytes(mail[0][1])
        time.sleep(1)
        try:
            #Gettin subj and text from body of eMail
            subject = decode_header(mail['Subject'])[0][0].decode('utf8')
            text = base64.b64decode(mail.get_payload()[0].get_payload()).decode('utf8')
            #print (msgText)
            #creating Message's text
            msgText = f'{subject}\n{text}'
            #appending in case there is few messages
            payload.append(msgText)
            time.sleep(1)
        except:
            print ("[IDLE]There is new msg, but i can't find necessary ALARM word")
    #returning list of messages to send
    return payload



def main():
    while True:
        number_to_send_sms = '79876543210'
        text = check_mail()
        #itereating through all msgs
        for i in text:
            if i != None:
                #Running MSG sender with predefined variables
                sendSMS = send_message(i, numberToSendMSG)
                print ('*****************')
                print (f'[SUCCESS]Message i sent via SMS:\n{i}')
                print(f'[INFO]http Responce: {send_message}')
                texted_responce = send_message.text
                json_responce = json.loads(texted_responce)
                print(f'[INFO]decoded http resp: {texted_responce}')
                message_id = json_responce['messages'][0]['internal_id']
                print ('*****************')
                time.sleep(2)
                #Saving msgID and msgText to file for history
                with open ('history.yaml', 'r') as history:
                    yaml_history = yaml.safe_load(history)
                with open ('history.yaml', 'w') as write_history:
                    yaml_history[message_id] = i
                    new_yaml_history = yaml.safe_dump(yaml_history, allow_unicode=True)
                    write_history.write(new_yaml_history)
        print("[IDLE]Don't have any unseen messages")

        time.sleep(2)

main()
